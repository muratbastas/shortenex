defmodule ShortenexWeb.Router do
  use ShortenexWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ShortenexWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/l/:shorten", LinksController, :find_by_shorten
    resources "/links", LinksController, only: [:show, :create], singleton: true
  end

  # Other scopes may use custom stacks.
  # scope "/api", ShortenexWeb do
  #   pipe_through :api
  # end
end
