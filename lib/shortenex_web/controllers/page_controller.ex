defmodule ShortenexWeb.PageController do
  use ShortenexWeb, :controller

  alias Shortenex.Links
  alias Shortenex.Links.Link

  @spec index(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def index(conn, _params) do
    render(conn, "index.html", changeset: Links.change_link(%Link{}))
  end
end
