defmodule ShortenexWeb.LinksController do
  use ShortenexWeb, :controller

  alias Shortenex.Links

  def show(conn, %{"id" => id} = _params) do
    link = Links.get_link!(id)

    render(conn, "show.html", link: link)
  end

  @spec show(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def find_by_shorten(conn, %{"shorten" => shorten} = _params) do
    link = Links.get_by_shorten!(shorten)

    render(conn, "show.html", link: link)
  end

  @spec create(Plug.Conn.t(), %{
          optional(:__struct__) => none(),
          optional(atom() | binary()) => any()
        }) :: Plug.Conn.t()
  def create(conn, %{"link" => link_params} = _params) do
    case Links.create_link(link_params) do
      {:ok, link} ->
        conn
        |> put_flash(:notice, "Link shortened successfuly!")
        |> redirect(to: Routes.links_path(conn, :find_by_shorten, link.shorten))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Link could not be shortened!")
        |> put_view(ShortenexWeb.PageView)
        |> render("index.html", changeset: changeset)
    end
  end
end
