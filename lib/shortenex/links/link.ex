defmodule Shortenex.Links.Link do
  use Ecto.Schema
  import Ecto.Changeset

  @dict "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

  schema "links" do
    field :original, :string
    field :shorten, :string

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:original, :shorten])
    |> validate_required([:original])
  end

  @spec create_shorten_from_id(any()) :: binary()
  def create_shorten_from_id(id) do
    length = String.length(@dict)

    id
    |> Integer.to_string()
    |> String.codepoints()
    |> Enum.reduce_while("", fn i, acc ->
      if i > 0 do
        {:cont, acc <> String.at(@dict, rem(String.to_integer(i), length))}
      else
        {:halt, div(String.to_integer(i), length)}
      end
    end)
    |> String.reverse()
  end

  # def while(acc, id, _fun) when id < 1, do: acc

  # def while(acc, id, fun) do
  #   fun.(id, acc)
  # end

  @spec get_id_from_shorten(binary()) :: any()
  def get_id_from_shorten(chars) do
    chars
    |> String.codepoints()
    |> Enum.map_join("", fn x ->
      Enum.find_index(String.codepoints(@dict), fn i ->
        i == x
      end)
    end)
    |> String.reverse()
  end
end
