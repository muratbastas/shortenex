defmodule Shortenex.Repo do
  use Ecto.Repo,
    otp_app: :shortenex,
    adapter: Ecto.Adapters.Postgres
end
