defmodule Shortenex.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :original, :string
      add :shorten, :string

      timestamps()
    end
  end
end
